# Ansible GitLab Zoekt

This repo includes an example ansible playbook that uses `gitlab-zoekt` role to provision zoekt nodes for the gitlab application to use for improved code search.

## Getting Started

### Ensure you have ansible installed

Refer to ansible documentation for installation instructions: https://docs.ansible.com/ansible/latest/installation_guide/index.html

### Clone this repository

```bash
git clone https://gitlab.com/johnmason/ansible-gitlab-zoekt.git ~/ansible-gitlab-zoekt
```

### Update the inventory file

Edit the `inventory.yml` file and include the IP addresses of any hosts you'd like to function as a GitLab zoekt node. For syntax on editing inventory file in yaml format, refer to ansible documentation: https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html

### Tweak the playbook

Make any changes necessary in `playbook.yml` before continuing.

#### Specify whether you want golang installed or not

The included playbook assumes your zoekt nodes don't have golang installed and runs an ansible role to install golang for you. If you already have golang installed, remove the `gantsign.golang` ansible role in `playbook.yml` and specify the `golang_gopath` variable to wherever you have golang installed.

```diff
7c7
<     golang_gopath: '/root/workspace-go'
---
>     golang_gopath: '/root/go'
12d11
<     - role: gantsign.golang
   
```

If you need golang installed you need the `gantsign.golang` role. You can install it with `ansible-galaxy role install gantsign.golang`.

#### Specify where your GitLab shell secret file is located

Zoekt nodes require GitLab's shell secret in order to authenticate successfully. This playbook assumes you have the shell secret in a file somewhere on your local file system. Specify the path to that file in the `gitlab_shell_secret_src_path` variable.

```diff
10c10
<     gitlab_shell_secret_src_path: ~/gitlab-development-kit/gitlab/.gitlab_shell_secret
---
>     gitlab_shell_secret_src_path: ~/path/to/your/gitlab/shell/secret/file

```

#### Specify the url location of where your GitLab application can be reached

Zoekt nodes need to be able to reach GitLab API directly. Specify the `gitlab_url` in the playbook to whatever your GitLab application is reachable.

```diff
9c9
<     gitlab_url: "http://192.168.100.100:3001"
---
>     gitlab_url: "https://yourgitlab.example.com"

```

### Run the playbook!

```bash
ansible-playbook -i inventory.yml playbook.yml
```
